import { Component } from '@angular/core';


@Component({
  selector: 'app-status-box',
  templateUrl: './status-box.component.html',
  styleUrls: ['./status-box.component.css']
})
export class StatusBoxComponent {
 ngSelect = 1;

   data = [
  {
    id:1, 
    isSelected: false ,
    name: "Dispatched"
  },
  { 
   id:2,
   isSelected: false ,
   name: "WareHouse"
   },
  { 
   id:3,
   isSelected: false ,
   name: "Delivered" 
  }
]

idSelected = null; 

changeColorOfselectedValue() {
 
//  this.data.map((val) =>  val.isSelected = val.id == this.idSelected)

if(this.idSelected == 1){
  this.data[0].isSelected = true
}
if(this.idSelected== 2 && this.data[0].isSelected == true){
  this.data[1].isSelected = true
}
if(this.idSelected == 3 && this.data[0].isSelected == true && this.data[1].isSelected == true){
  this.data[2].isSelected = true
}

}
}
